#include <iostream>
#include "data.h"
#include "parser.h"
#include "logic.h"

int main()
{
DataBase data;
Parser parser(&data);
Logic logic(&data);
std::string path;
std::cout<<"path to rules: ";
std::cin>>path;
parser.readFile(path);
logic.doResolution(data.target);
//logic.tryUnification(data.rules[0].results[0],data.rules[0].reasons[0]);

return 0;
}


