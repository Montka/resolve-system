#ifndef LOGIC_H
#define LOGIC_H
#include "terms.h"
#include <list>
#include <vector>
#include <string>

class Logic
{

class Unify_expt
{
public:
std::string msg;
Unify_expt(std::string str):msg(str)
{}
};

class Resolution_sucsees
{
public:
 Resolution_sucsees(){}
};



struct Substitusion   //Substitusion
{
Substitusion(Term* r1, Variable* r2):replacement(r1),replacing(r2)
{}
Term* replacement;
Variable* replacing;
};

struct Resolventa
{
std::list<Relation*> negative;
std::list<Relation*> positive;
std::vector<Substitusion> substitusions;
Resolventa(){}
};

DataBase* database_ptr;
Term* findSubstitusion(Variable*,std::vector<Substitusion>*);
void unifyRelations(Relation const*, Relation const*, std::vector<Substitusion>*);
Relation* copyRelation(Relation*);
void deleteRelation(Relation*);
bool tryUnification(Relation const*, Relation const*, std::vector<Substitusion>&);
std::string nameResolventa(Resolventa const&);
void findResolventa(Rule&,Resolventa&, std::list<Resolventa>&);
public:
Logic(DataBase*);
void doResolution(Rule);

};
#endif
