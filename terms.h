#ifndef TERMS_H
#define TERMS_H
#include <vector>
#include <list>
#include <string>
enum TermType
{TERM,CONST,VARIABLE,RELATION};

class Term
{
public:
std::string name;
TermType type;
virtual void addArg(Term* ptr)=0;
};
class Const: public Term
{
virtual void addArg(Term* ptr ){}
};

class Variable : public Term
{
virtual void addArg(Term* ptr){}
};

class Relation : public Term
{
public:
std::vector<Term*> args;
virtual void addArg(Term* ptr){args.push_back(ptr);}
};

struct Rule
{
std::list<Relation*> reasons;
std::list<Relation*> results;
};
#endif
