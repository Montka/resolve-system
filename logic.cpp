#include "data.h"
#include "logic.h"
#include <iostream>

Term* Logic::findSubstitusion(Variable* var,std::vector<Substitusion>* list)
{
Term* tmp=var;

while(tmp->type==VARIABLE)
{
for(auto i: *list)
if(i.replacing==tmp) {tmp=i.replacement;continue;}
break;
}
return tmp;
}


void Logic::unifyRelations(Relation const* first, Relation const* second, std::vector<Substitusion>* list)
{ 
if(first->name!=second->name) throw Unify_expt("Differnt names");
if(first->args.size()!=second->args.size()) throw Unify_expt("Different argument amount");
Term* term1;
Term* term2;

for(int i=0;i<first->args.size();++i)
{
term1=first->args[i];
term2=second->args[i];

if(term1->type==VARIABLE) term1=findSubstitusion(dynamic_cast<Variable*>(term1),list);

if(term2->type==VARIABLE) term2=findSubstitusion(dynamic_cast<Variable*>(term2),list);

if(term1==term2) continue;
if(term1->type==VARIABLE || term2->type==VARIABLE)
	switch(term1->type)
	{
	case CONST: {list->emplace(list->begin(),term1,dynamic_cast<Variable*>(term2));break;}
	case RELATION: {list->emplace(list->begin(),term1,dynamic_cast<Variable*>(term2));break;}
	case VARIABLE: {list->emplace(list->begin(),term2,dynamic_cast<Variable*>(term1));break;}
	}
else if(term1->type==term2->type)
	{
		switch(term1->type)
		{
		case CONST:{if(term1!=term2) throw Unify_expt("Constants doesnt match");break;}
		case RELATION:{unifyRelations(dynamic_cast<Relation*>(term1),dynamic_cast<Relation*>(term2),list);break;}
		}
	
	}
else throw Unify_expt("No possible unification");
}
}

Relation* Logic::copyRelation(Relation* rel)
{
Relation* copy=new Relation;
copy->name=rel->name;
for(auto i:rel->args)
{
if(i->type==RELATION)
copy->args.push_back(copyRelation(dynamic_cast<Relation*>(i)));
else
copy->args.push_back(i);
}
return copy;
}

void Logic::deleteRelation(Relation* rel)
{
for(auto i:rel->args)
if(i->type==RELATION)
deleteRelation(dynamic_cast<Relation*>(i));
delete rel;
}







bool Logic::tryUnification(Relation const* one, Relation const* two, std::vector<Substitusion>& list)
{
//std::cout<<database_ptr->nameTerm(one)<<" & "<<database_ptr->nameTerm(two)<<std::endl;
if(one->name!=two->name) return false;
if(one->args.size()!=two->args.size()) return false;
std::vector<Substitusion> list_tmp=list;
try
{
unifyRelations(one,two,&list_tmp);
}
catch(Unify_expt expt){std::cout<<expt.msg<<std::endl; return false;}

for(auto i:list_tmp)
std::cout<<database_ptr->nameTerm(i.replacement)<<"/"<<database_ptr->nameTerm(i.replacing)<<", ";
std::cout<<std::endl;
list=list_tmp;

return true;
}

std::string Logic::nameResolventa(Resolventa const& resolve)
{
std::string tmp;
for(Relation* l:resolve.negative)
tmp+=database_ptr->nameTerm(l)+",";
tmp+="->";
for(Relation* r:resolve.positive)
tmp+=database_ptr->nameTerm(r)+",";
return tmp;
}
template<typename T>
void removeOnce(T obj,std::list<T>& list)
{
for(typename std::list<T>::iterator i= list.begin(); i!=list.end();++i)
{if(*i==obj) list.erase(i);return;}

}

void Logic::findResolventa(Rule & rule,Resolventa & resolve, std::list<Resolventa> &list)
{
Relation* relation=resolve.negative.front();
//for(Relation* relation:resolve.negative)
	for(Relation* result:rule.results)
		{
	
		if(tryUnification(relation,result,resolve.substitusions))
		{
		
		Resolventa tmp = resolve;
		for(Relation* result_t:rule.results)
			if(result_t!=result)
			tmp.positive.push_back(result_t);

		for(Relation* reason:rule.reasons)
			tmp.negative.push_back(reason);	
		
		removeOnce(relation, tmp.negative);
		
		std::cout<<database_ptr->nameRule(rule)<<"  V  "<<nameResolventa(resolve)<<std::endl;
		std::cout<<nameResolventa(tmp)<<"  "<<list.size()<<"  "<<std::endl;
		if(tmp.positive.size()==0 && tmp.negative.size()==0 )
		{std::cout<<"NIL resolved"<<std::endl;throw Resolution_sucsees();}
		else
		list.push_back(tmp);
		}
		}
	/*
for(Relation* relation:resolve.positive)
	for(Relation* reason:rule.reasons)	
		if(tryUnification(relation,reason,resolve.substitusions))
		{
		Resolventa tmp = resolve;
		
		for(Relation* reason_t:rule.reasons)
			if(reason_t!=reason)
			tmp.negative.push_back(reason_t);	
			
		for(Relation* result:rule.results)
			tmp.positive.push_back(result);	
		
		tmp.positive.remove(relation);	
		std::cout<<nameResolventa(tmp)<<"  "<<list.size()<<"  "<<std::endl;
		if(tmp.positive.size()==0 && tmp.negative.size()==0 )
		{std::cout<<"NIL resolved"<<std::endl;throw Resolution_sucsees();}
		else
		list.push_back(tmp);
		}*/
}
 



Logic::Logic(DataBase* ptr)
{database_ptr=ptr;}

void Logic::doResolution(Rule target)
{
std::list<Resolventa> resolv_list;
target.reasons.swap(target.results);
/*
for(auto rule:database_ptr->rules)
{
Resolventa tmp;
for(Relation* reason:rule.reasons)
tmp.negative.push_back(reason);

for(Relation* result:rule.results)
tmp.positive.push_back(result);

resolv_list.push_back(tmp);

//tmp.positive.clear();
//tmp.negative.clear();
}
*/
Resolventa tmp;
for(Relation* reason:target.reasons)
tmp.negative.push_back(reason);

for(Relation* result:target.results)
tmp.positive.push_back(result);

resolv_list.push_back(tmp);


while(!resolv_list.empty())
{
//resolv=resolv_list.front();
try {
	for(Rule rule:database_ptr->rules)
		findResolventa(rule,resolv_list.front(),resolv_list);
				
		findResolventa(target,resolv_list.front(),resolv_list);
		std::cout<<resolv_list.size()<<std::endl;	
	} catch (Resolution_sucsees tmp){std::cout<<"Theorem proved"<<std::endl; return;}
		resolv_list.pop_front();
		database_ptr->rules.push_back(database_ptr->rules.front());
		database_ptr->rules.pop_front();
		
}
std::cout<<"All resolvents passed"<<std::endl;

}

