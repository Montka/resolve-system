#ifndef PARSER_H
#define PARSER_H
#include "terms.h"
#include <list>
#include <string>
class Parser
{

std::list<Term*> tmpStorage;
DataBase* database_ptr;

Relation* parseRelation(std::string);
template<typename T> 
T* findIn(std::list<T*>, std::string);
bool parseWithWildcard(std::string, std::string);
void trim(std::string&);
Rule parseString(std::string);
size_t findConsBrackets(std::string, char, int);
Term* parseArgs(std::string, int);
public:
Parser(DataBase*);
~Parser();
bool readFile(std::string);
};
#endif
