#include "data.h"

std::string DataBase::nameTerm(Term const* term)
{
std::string str=term->name;

if(term->type==RELATION)
{
Relation* ptr=dynamic_cast<Relation*>(const_cast<Term*>(term));

str+="(";
for(auto i:ptr->args)
str+=nameTerm(i)+", ";
str+=")";

}

return str;
}



std::string DataBase::nameRule(Rule& rule)
{
std::string tmp;
for(auto i:rule.reasons)
tmp+=nameTerm(i)+" & ";
tmp+=" -> ";
for(auto i:rule.results)
tmp+=nameTerm(i)+" & ";
return tmp;
}


DataBase::~DataBase()
{
for(auto i: storage)
delete i;
}


