#include "data.h"
#include "parser.h"
#include <iostream>
#include <fstream>

template<typename T> 
T* Parser::findIn(std::list<T*> vec, std::string str)
{
for(auto i:vec)
if(i->name==str) return i;
return NULL;
}



bool Parser::parseWithWildcard(std::string str, std::string wildcard)
{
unsigned int str_pos=0;
unsigned int i=0;
while(i!=wildcard.size())
{
if(wildcard[i]=='*')
	{
	unsigned int j=str_pos;
	for(str_pos=str.size();str_pos>j && str[str_pos]!=wildcard[i+1]; --str_pos){}}
	
	
else if(wildcard[i]=='#')
	for(;str_pos<str.size() && str[str_pos]!=wildcard[i+1] && str[str_pos]==' '; ++str_pos){}
	
	
else if(wildcard[i]=='@')
	for(;str_pos<str.size() && str[str_pos]!=wildcard[i+1] && ((str[str_pos]>64 &&str[str_pos]<91)||(str[str_pos]>96 &&str[str_pos]<123) || (str[str_pos]>47 && str[str_pos]<58) || str[str_pos]=='_'); ++str_pos){}
		
	
else if(wildcard[i]=='?')
	{if(str_pos>=str.size()) return false;
	++str_pos;}
	
else if(str[str_pos]!=wildcard[i])
	{return false;}
	else {if(str_pos>=str.size()) return false;
	++str_pos;}
++i;
}
if(str_pos<str.size()) return false;
return true;
}

bool Parser::readFile(std::string name)
{
std::ifstream fs;
std::string buffer;
fs.open(name.c_str());
if(!fs)
{return false;}
while(fs)
{
std::getline(fs,buffer);
if(buffer.size()==0) continue;
std::cout<<std::endl<<"New string:"<<std::endl<<buffer<<std::endl;
if(parseWithWildcard(buffer,"#//*")) continue;
if(parseWithWildcard(buffer,"#Target:*")) database_ptr->target=parseString(buffer.substr(buffer.find_first_of(":")+1,-1));
else
database_ptr->rules.push_back(parseString(buffer));//;
}
}

void Parser::trim(std::string& str)
{
str.erase(0, str.find_first_not_of(' '));       
if(!str.empty()) str.erase(str.find_last_not_of(' ')+1);         
}

Rule Parser::parseString(std::string str)
{
trim(str);
std::string part;
bool target=false;
Rule rule_tmp;

std::size_t found = str.find("->", 0);
if(found==-1)
{
std::cout<<"Result part:"<<std::endl;
part=str.substr(0,-1);
rule_tmp.results.push_back(parseRelation(part));

}
else
{
found = str.find_first_of("&");

while (found!=std::string::npos)
  {
    part=str.substr(0,found);
   
    str.erase(0,found+1);
    found=str.find_first_of("&");
    
    rule_tmp.reasons.push_back(parseRelation(part));
  }
  
found = str.find("->", 0);
part=str.substr(0,found);
rule_tmp.reasons.push_back(parseRelation(part));
str.erase(0,found+2);
part=str.substr(0,-1);
std::cout<<"Result part:"<<std::endl;
rule_tmp.results.push_back(parseRelation(part));

}
tmpStorage.clear();
return rule_tmp;
//database_ptr->rules.push_back(rule_tmp);
}

size_t Parser::findConsBrackets(std::string str, char ch, int n=0)
{
int brackets=0;
for(int i=n;i<str.size();++i)
{
if(str[i]==ch && brackets==0){return i;}
if(str[i]=='(') ++brackets;
if(str[i]==')') --brackets;
if(brackets<0) return -1;
}
return -1;
}
Term* Parser::parseArgs(std::string str, int depth)
{
++depth;
Term* ptr;
for(int i=0; i<depth;++i)std::cout<<"  ";
std::cout<<depth<<" ";
trim(str);
if(parseWithWildcard(str,"$*"))	
	{
	std::cout<<"variable: "<<str;
	ptr=findIn(tmpStorage,str);
	if(ptr==NULL)
	{
	std::cout<<" New variable, added"<<std::endl;
	ptr=new Variable;
	ptr->name=str;
	ptr->type=VARIABLE;
	tmpStorage.push_back(ptr);
	database_ptr->storage.push_back(ptr);
	}
	else{std::cout<<" Variable exists"<<std::endl;}
	}

else if(parseWithWildcard(str,"@(*)"))
	{
	std::size_t found= str.find_first_of("(");
	std::string name=str.substr(0,found);
	std::cout<<"Relation: "<<name<<std::endl;
	
	ptr=findIn(database_ptr->storage,name);
	//if(ptr==NULL)
	//{
	//std::cout<<" New relation"<<std::endl;
	//}else{std::cout<<" Relation with same name exists"<<std::endl;}
	
	ptr=new Relation;
	ptr->name=name;
	ptr->type=RELATION;
	database_ptr->storage.push_back(ptr);
	//database_ptr->relations.insert(std::pair<std::string,Relation*>(name,dynamic_cast<Relation*>(ptr)));
	str.erase(0,found+1);
	
	trim(str);
	
	std::string part;
	found=findConsBrackets(str, ',');
	while (found!=std::string::npos)
  	{
    	part=str.substr(0,found);
	str.erase(0,found+1);
	found=findConsBrackets(str,',');
	ptr->addArg(parseArgs(part,depth));
	}
  	part=str.substr(0,str.size()-1);
	ptr->addArg(parseArgs(part,depth));
	}
else if(parseWithWildcard(str,"@"))
	{
	std::cout<<"const: "<<str;
	ptr=findIn(database_ptr->storage,str);
	if(ptr==NULL)
	{
	std::cout<<" New constant, added"<<std::endl;
	ptr=new Const;
	ptr->name=str;
	ptr->type=CONST;
	database_ptr->storage.push_back(ptr);
	}
	else{std::cout<<" Constant exists"<<std::endl;}
	}
	
else 
{
std::cout<<"err"<<std::endl;
}
--depth;
return ptr;	
}

Parser::Parser(DataBase* ptr)
{
database_ptr=ptr;
}
Parser::~Parser()
{}


Relation* Parser::parseRelation(std::string str)
{
Relation* ptr;
trim(str);
//std::cout<<str<<std::endl;
if(!parseWithWildcard(str,"@(*)"))
{
std::cout<<"ERROR: "<<str<<std::endl;
return NULL;
}
ptr=dynamic_cast<Relation*>(parseArgs(str,0));

if (ptr==NULL)
std::cout<<"Error";

return ptr;

}
