CC=g++
FLAGS=-std=c++11 -O3
OUTPUT=test
OBJECTS=Logic.o Data.o Parser.o

all:build
	
build: $(OBJECTS)
	$(CC) main.cpp $(OBJECTS) -o $(OUTPUT) $(FLAGS)

Logic.o: logic.cpp logic.h
	$(CC) -c logic.cpp -o Logic.o $(FLAGS) 

Data.o: data.cpp data.h
	$(CC) -c data.cpp -o Data.o $(FLAGS)
	
Parser.o: parser.cpp parser.h
	$(CC) -c parser.cpp -o Parser.o $(FLAGS)
	
clear:
	rm -f *.o
