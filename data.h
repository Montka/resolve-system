#ifndef DATA_H
#define DATA_H
#include "terms.h"
#include <list>
#include <string>
class DataBase
{
public:
std::list<Term*> storage;
std::list<Rule> rules;
Rule target;
std::string nameTerm(Term const*);
std::string nameRule(Rule&);
~DataBase();
};
#endif
